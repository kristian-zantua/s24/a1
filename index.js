//2
db.users.find(
	{
		$or:[
			{"firstName": {$regex: "s", $options: "i"}} 
			{"lastName": {$regex: "d", $options: "i"}}
		]
	
	{
		"_id":0,
		 "firstName":1, 
		 "lastName":1
	}
);
//3
db.users.find(
	{
		"department":"HR"
	},
	$and: 
	{
		"age": {$gte:70}
	}
	
);
//4
db.users.find(
	{
		$and:
		{"firstName": {$regex: "e"},
		{"age": {$lte:30}}
	},
)